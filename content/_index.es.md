---
---

{{% frontheader "¿Por qué frenar los combustibles fósiles?" why %}}

{{< figure src="/img/houston-flood-august-2017.jpg" caption="Huracán Harvey — Houston, Tejas, EE.UU. — agosto 2017" class="center section" >}}

{{% details climate-change %}} 
{{< summary >}}
**El cambio climático está acabando con el planeta** y continuará empeorando cada vez más. Considerar la posibilidad de que contamos con un "presupuesto de carbono" para años venideros es un delirio ilusorio. El día de hoy ya tenemos una deuda de carbono que debemos remover de la atmósfera.
{{< /summary >}}

Actualmente, el cambio climático está aumentando la frecuencia y severidad de [incendios](https://www.ucsusa.org/global-warming/science-and-impacts/impacts/global-warming-and-wildfire.html#.W15Zr9L0mUm), [inundaciones](http://sealevel.climatecentral.org/uploads/research/Unnatural-Coastal-Floods-2016.pdf), [sequías](http://www.cgd.ucar.edu/cas/adai/papers/Dai-drought_WIRES2010.pdf) y [huracanes](https://www.ucsusa.org/global-warming/science-and-impacts/impacts/hurricanes-and-climate-change.html). El [nivel de agua](https://nca2014.globalchange.gov/report/our-changing-climate/sea-level-rise) ha aumentado 20 cm y continuará subiendo. La acidez de los océanos ha [incrementado en un 30%](https://ocean.si.edu/ocean-life/invertebrates/ocean-acidification) y los arrecifes coralinos están sufriendo [blanqueamientos sin precedentes](http://www.climatesignals.org/headlines/events/global-coral-bleaching-2014-2017). A cada año desde el 2008 al 2017, [21.3 millones de personas](http://www.internal-displacement.org/global-report/grid2018/downloads/2018-GRID.pdf) se han visto desplazadas de sus territorios para vivir como refugiados por efectos del cambio climático y sus desastres relacionados. Es probable que la cifra aumente a [cientos de millones](https://www.iom.int/complex-nexus#estimates) de refugiados climáticos para el 2050.

La devastación que actualmente experimentamos es el resultado de tan sólo 1°C de aumento en el calentamiento global desde los años ochenta. Décadas atrás, la [comunidad científica definió](http://www.climatechangenews.com/2017/08/23/end-2c-climate-limit/) que la opción más segura limitaría el incremento a sólo 1°C, pero se le consideró inalcanzable. El Acuerdo de París busca "quedar por debajo" de los 2 °C que se seleccionó de forma completamente arbitraria y sus promotores repetidamente aseguran que puede mantenerse el objetivo alcanzable por debajo de 1.5°C. 

Dejando las pretensiones políticas de lado, aunque frenaramos hoy mismo por completo la quema de combustibles fósiles, ya tenemos asegurado un aumento de 1.5 a 2°C:

- La Tierra ya se ha calentado en **1°C**.
- La quema de combustibles fósiles produce [aerosoles](http://www.climatecodered.org/2018/02/quantifying-our-faustian-bargain-with.html) que enfrían la atmósfera automáticamente. Al frenar su liberación al aire (hecho que debe suceder para limitar daños), la Tierra aumentará su temperatura de forma inmediata entre 0.5°C y 1.1°C, teniendo por estimación más probable **0.7°C**.
- Los efectos de retroalimentación que aumentan la temperatura (como el derretimiento de los hielos perennes, la reducción del área blanca y reflectora de los polos, el colapso de las poblaciones de fito-plancton y la pérdida de vegetación por sequías e incendios) tendrán un fuerte impacto que aumentará esta cifra.

Inevitablemente, las condiciones que ya son muy peligrosas para millones de personas van a empeorar. Es muy simple, no podemos aspirar a quemar más combustibles fósiles y liberar su carbono a la atmósfera. Tenemos el imperativo moral de poner un alto a los combustibles fósiles **ahora**.

{{% /details %}}

{{% details killing-humans %}} 
{{< summary >}}
**La contaminación y cambio climático asesinan seres humanos**: mueren anualmente más de 6 millones de personas y la cifra continúa escalando.
{{< /summary >}}

En el 2015 la contaminación industrial aérea, terrestre, bioquímica y laboral mataron a 5.5 millones de seres humanos. En promedio, desde el año de 1990, cada año la tasa ha ido aumentando en 50,000 muertes humanas.

([*Comisión Lancet* de contaminación y salud](http://www.thelancet.com/pdfs/journals/lancet/PIIS0140-6736(17)32345-0.pdf), figura 7)

Del 2012 a la fecha de hoy, el cambio climático ha asesinado a 400,000 seres humanos cada año. Las proyecciones estiman que la cifra aumentará a 700,000 muertes anuales para el año 2030. ([Monitor de Vulnerabilidad DARA](https://daraint.org/wp-content/uploads/2012/09/CVM2ndEd-FrontMatter.pdf), 2a ed)
{{% /details %}}

{{% details ecological-collapse %}} 
{{< summary >}}
**Presenciamos un colapso ecológico global**, ocasionado y fomentado por los combustibles fósiles. Bosques, praderas y océanos, que son la matriz de la vida, están en condiciones críticas.
{{< /summary >}}

A cada etapa de su uso, los combustibles fósiles destruyen los ecosistemas de los que la vida depende: desde su extracción, procesamiento y transporte hasta su uso y daños colaterales.

Como ejemplo están las arenas alquitranadas de Alberta, donde máquinas gigantescas destruyen la superficie de la tierra dejando atrás sedimentos de lo que alguna vez fueron montañas, sepultando ríos con los escombros desechados y fragmentando el hábitat con millones de pozos. Como resultado, se ha creado el proyecto industrial más grande del planeta, un verdadero infierno en la tierra. Las refinerías y plantas de procesamiento de gas contaminan los alrededores. Los oleoductos continuamente tienen derrames de sustancias tóxicas, tan sólo en EE.UU. hay [más de "300 accidentes significativos por año"](http://www.biologicaldiversity.org/campaigns/americas_dangerous_pipelines/).

{{< figure src="/img/bayan-obo-rare-earths-mine-s.jpg" caption="Mina de tierras raras Bayan Obo" link="/img/bayan-obo-rare-earths-mine.jpg" class="right noPDF" >}}

Mucho peor que el daño ambiental directo es la actividad industrial que los combustibles fósiles hacen posible. El mundo ha perdido cerca del 80% de los bosques primarios y {{< pagelink "cada dos segundos perdemos un acre de bosque natural" "/ecological-collapse/forest-loss" >}}. La agricultura y ganadería industrializadas transforman vastos y complejos hábitats en monocultivos para uso humano, contribuyen a la proliferación de algas y [erosionan la capa superior de suelo](http://www.pnas.org/content/104/33/13268.full) a una tasa de 10 a 100 veces mayor a la de reposición. Las minas a cielo abierto, además de devastar la tierra, generan estanques gigantescos de residuos tóxicos dejando destrucción visible desde el espacio exterior. Las [operaciones industriales](https://www.nature.com/articles/ncomms10244) arrasan con 80% de la pesca anual, dando como resultado que el [84% de los recursos pesqueros](http://www.seaaroundus.org/data/#/global/stock-status) se encuentran actualmente completamente explotados, sobre explotados o en colapso. 

Las presiones ecológicas generadas por los combustibles fósiles han ocasionado la sexta extinción masiva y {{< pagelink "exterminio biológico" "/ecological-collapse/biological-annihilation" >}}. En el 2014 la Tierra había perdido el 60% de las poblaciones de vertebrados y probablemente una cifra comparable de insectos y plantas; este número toma como base de referencia información de 1970, la cual ya estaba severamente diezmada por todas las extinciones previas. Para detener una pérdida mayor de vida silvestre debemos de poner un alto a los combustibles fósiles.

{{% /details %}}

{{% details those-we-love %}} 
{{< summary >}}
Nuestra labor es monumental. Por cada paso que el movimiento ambientalista da para adelante, el cambio climático y la economía industrial nos empuja diez para atrás. Ya sea que tu pasión sea una comunidad ecológica, una especie en peligro de extinción o las generaciones del futuro, **debemos poner un alto a los combustibles fósiles para defender a quienes amamos**.
{{< /summary >}}

Los ambientalistas a nivel comunitario trabajamos ardua y ferozmente por los lugares y criaturas que amamos. Ganamos algunas batallas importantes aquí y allá, pero de forma general estamos perdiendo la guerra. A pesar de décadas de ambientalismo, en todo el mundo cada comunidad ecológica está en declive y el cambio climático amenaza a cada lugar y especie que hemos sido capaces de proteger.

Estamos en esta lucha porque hemos optado por llevar a cabo batallas defensivas con ganancias temporales y pérdidas permanentes. Podemos retrasar la tala de un bosque, la construcción de una refinería, el desarrollo de un complejo industrial u oleoducto por un año, pero aquellos que obtienen las ganancias de la destrucción regresarán el próximo, y el que le sigue, hasta que obtengan lo que quieran. Una vez que el bosque es talado, los territorios acaban arrasados, se construye la refinería o se instala el oleoducto, los daños son irreparables.

Relativamente somos pocos, pero por cada proyecto que logramos bloquear, dejamos que muchos otros procedan sin oposición. Por ejemplo, en los EEUU, los efectos masivos de los ambientalistas comunitarios sólo lograron retrasos temporales para aplazar la construcción de los oleoductos de Keystone XL y Dakota, que combinados expandieron la línea de oleoductos en más de 3,200 km. Mientras tanto los EE.UU. completaron 27,300 km de oleoductos entre el 2009 y 2014. A principios del 2016, había 37,000 kilómetros [en obra o planeación](https://ucononline.com/2016/01/2016-north-american-pipeline-outlook/).

Los combustibles fósiles son la causa raíz que amenaza a los lugares y seres que amamos. El ataque no se detendrá hasta que cambiemos de nuestras acciones defensivas y reaccionarias (que en el mejor de los casos sólo obtienen victorias temporales) a elegir batallas ofensivas que afrenten el problema de raíz.

{{% /details %}}

{{% details hope-not-enough %}} 
{{< summary >}}
El movimiento ambientalista ha trabajado por décadas hacia un despertar masivo de conciencia, pero, aun así, todo continúa empeorando mientras perdemos nuestro planeta. **No podemos depender de la esperanza** de que este patrón de comportamiento va a cambiar repentinamente.
{{< /summary >}}

Con la esperanza de lograr una adopción masiva de modos de vida sostenibles, el movimiento ambientalista ha intentado arduamente de convencer a los individuos que cambien sus valores y estilos de vida. Aunque algunas personas han respondido al llamado a cambiar, permanecen como una diminuta minoría. Todas las medidas globales de sostenibilidad se encaminan en la dirección equivocada. Décadas de ambientalismo no han podido reducir el crecimiento de la población, consumo, minería, generación de desperdicio y concentración de dióxido de carbono atmosférico, todos continúan en aumento desbocado.

De manera racional, sabemos que la quema del carbón que ha sido secuestrado durante millones de años es un error. Pero resulta natural para individuos y comunidades, ya sean humanas, bosques o praderas, emplear por completo toda la energía y alimento disponibles. Los combustibles fósiles son depósitos de energía increíblemente densos, así que no es de sorprender que hayamos desarrollado una adicción absoluta. Aunque algunos cuantos individuos puedan elegir renunciar a esta energía accesible, la gran mayoría quemará cuanto le sea permitido durante el mayor tiempo que les sea posible.

No existe evidencia de un cambio de consciencia masivo, y pudiendo perder el mundo entero, no podemos permitirnos creer en ilusiones. La única manera en la que la sociedad industrial dejará de quemar combustibles fósiles reside en que ya no tenga acceso a ellos. 

{{% /details %}}

{{% details green-tech-not-enough %}} 
{{< summary >}}
La energía renovable está creciendo a tasas sin precedentes, pero ni siquiera logra retrasar el aumento de la quema de combustibles fósiles. **La tecnología verde no es solución**.
{{< /summary >}}

{{< figure caption="Naranja: incremento en el consumo global de energía desde el 2009. Verde: incremento en energía hydrológica, eólica, solar, biocombustibles, geotérmica y biomasa." attr="Barry Saxifrage" attrlink="https://www.nationalobserver.com/2017/09/20/analysis/fossil-fuel-expansion-crushes-renewables" src="/img/bp-global-energy-demand-v-renew-2.jpg" alt="Demanda vs Renovables: el incremento de energía global de los últimos siete años" class="right max50percentPDF" >}}

La energía renovable sólo podría ser una solución verdadera si realmente reemplazara a los combustibles fósiles, pero como explica detalladamente Barry Saxifrage, "la nueva forma de 'seguir con lo mismo' es una en la que [continuamos expandiendo ambas al mismo tiempo, las energías renovables y combustibles fósiles](https://www.nationalobserver.com/2017/09/20/analysis/fossil-fuel-expansion-crushes-renewables)." La sociedad industrial responde a la crisis ecológica y climática de la misma forma que un adicto consume todo lo que puede, usa toda la energía limpia o sucia, en lugar de adoptar un control racional sobre sus decisiones.

Del 2009 al 2016 el aumento global de energía incrementó en un 15%. Las energías renovables nuevas suministraron menos del 30% del incremento en la demanda, siendo la gran mayoría suministrada por combustibles fósiles. Y si no contamos las presas hidroeléctricas que asesinan a los ríos y sus habitantes, que además desplazan a millones de personas de sus territorios con el propósito de obtener energía "renovable", entonces el 80% del aumento en la demanda se logró con el consumo de energía sucia.

No podemos esperar a que se instalen "suficientes" fuentes de energía renovable antes de empezar la transición; no tenemos tiempo que perder, ni podemos confiar en que el aumento en la energía renovable podrá disminuir el consumo de combustibles fósiles. No importa cuántas celdas solares y turbinas eólicas se instalen, debemos de poner un alto a los combustibles fósiles lo más pronto posible.

{{% /details %}}

{{% details efficiency-not-enough %}} 
{{< summary >}}
Paradójicamente, la **eficiencia energética incrementa el consumo de combustibles fósiles**. Cuando obtienes un mayor rendimiento con el mismo insumo terminas incentivando el uso de ese recurso.
{{< /summary >}}

{{< figure src="/img/global-energy-consumption2.png" caption="Consumo global de energía desde 1900" class="right max50percentPDF" attr="The Shift Project Data Portal" attrlink="http://www.tsp-data-portal.org/Energy-Production-Statistics#tspQvChart" attrtarget="_blank" >}}

Al igual que con la tecnología verde, la eficiencia energética sólo se vuelve una solución verdadera si termina reduciendo el consumo real de combustibles fósiles. Una familia con una necesidad específica, tal como iluminar su casa cada noche, usará menos electricidad con un foco LED. Pero debido a que la sociedad de consumo tiene deseos ilimitados, la misma tecnología de iluminación termina por colonizar la ropa, los espectaculares y las fachadas de edificios enteros. No sólo termina por aumentar el consumo de energía, pero se [dispara la contaminación luminosa](https://gizmodo.com/the-switch-to-outdoor-led-lighting-has-completely-backf-1820652615).

Las [consecuencias no intencionadas](https://thetyee.ca/Opinion/2018/02/26/Energy-Efficiency-Curse/) hacen que dichas medidas de eficiencia sean contraproducentes. A pesar de las tecnologías y trucos que hemos desarrollado durante el último siglo, el consumo global de energía ha aumentado exponencialmente. Claramente, la eficiencia por sí misma no terminará con el uso de los combustibles fósiles. Peor aún porque termina por amplificar sus daños. [Richard York](https://www.youtube.com/watch?v=XHWefRvG7Lo&t=4m13s) lo ilustra con ayuda de un experimento mental sencillo: 

> Imagina dos mundos. En uno, los automóviles obtienen 50 km por cada litro de gasolina; en el otro usan 50 litros para recorrer un kilómetro. ¿Qué mundo crees que usa más energía?

En el mundo de vehículos ineficientes, los humanos nunca tendrían ni los motivos ni medios para construir sus casas lejos de sus trabajos, ni de centros comerciales aprovisionados por cadenas de abastecimiento globales, las cuales necesitan estar conectadas por vehículos, carreteras y autopistas. En su lugar construirían sociedades optimizadas para andar a pie y aliviar sus necesidades básicas de forma local. Paradójicamente, el mundo con los automóviles más eficientes daría paso (como lo podemos observar) al desarrollo de una red de máquinas hambrientas que consumen gasolina sin freno. 

Se nos ofrece la eficiencia como si fuera una alternativa para [desechar la opción de un cambio fundamental](http://www.lowtechmagazine.com/2018/01/bedazzled-by-energy-efficiency.html), pero "lograr hacer más" mientras continúas consumiendo toda la energía que puedes obtener no es ninguna solución. Acabar con el consumo de los combustibles fósiles debe ser el objetivo principal y de forma secundaria, la eficiencia podrá tomar un papel importante en esa transición. 
{{% /details %}}

{{% details politicians-not-enough %}} 
{{< summary >}}
**Los gobiernos no han adoptado medidas significantes** para reducir el consumo de combustibles fósiles, y no dan indicaciones de que lo harán.
{{< /summary >}}

{{< figure src="/img/carbon-emissions-atmospheric-concentration-s.png" caption="Inacción gubernamental conforme se acumula el carbono" class="right max50percentPDF" attr="NOAA and Global Carbon Budget" attrlink="/politicians-not-enough/carbon-emissions-concentration/" link="/politicians-not-enough/carbon-emissions-concentration/" >}}

Aun cuando la [comunidad científica ha advertido por décadas](https://history.aip.org/history/climate/timeline.htm) de los peligros del cambio climático, los gobiernos han sobresalido sólo para formar organizaciones con títulos oficiales, realizar conferencias, fomentar la investigación y anunciar con orgullo acuerdos meramente simbólicos. Aun así, por algún motivo, no han reducido las emisiones de carbono. Nunca. No sólo fracasaron al no adoptar el principio de precaución mientras los científicos debatían la severidad del problema del calentamiento global, pero no tomaron ninguna acción real, incluso cuando el consenso científico dejó en claro que se acercaba una catástrofe.

El logro más reciente, el Acuerdo de París, no es obligatorio, y se limita a proponer reducciones hipotéticas a las emisiones del futuro. Como con el Acuerdo de Copenhagen que le precedió, los gobiernos no tienen capacidad de hacer algo realmente sustancioso. Mientras tanto, los inversionistas continúan derramando dinero para la expansión global en un [30% de la energía que proviene de la quema de carbón](https://endcoal.org/global-coal-plant-tracker/); los inversionistas más inteligentes no creen que los gobiernos vayan a detener la quema del carbón en ningún momento próximo. 

Todos entendemos que los gobiernos cuidan más a las corporaciones que de los seres humanos. Los combustibles fósiles son la sangre vital para la economía industrial. Seríamos estúpidos si creyéramos que los políticos decidirán repentinamente hacer lo correcto después de todo este tiempo. Debemos poner un alto a los combustibles fósiles nosotros mismos.

{{% /details %}}

{{% details peak-oil-not-enough %}} 
{{< summary >}}
Los combustibles fósiles son recursos finitos, así que inevitablemente su uso se reducirá. Pero el **cenit petrolero no reducirá su consumo a tiempo para evitar la catástrofe**.
{{< /summary >}}

No podemos sostener la extracción de petróleo, gas natural y carbón que nuestra economía de crecimiento necesita cada vez más. Una vez que el [cenit petrolero](http://peakoil.com/) nos obligue a reducir su consumo, veremos repetirse la crisis financiera global del 2008, pero de forma permanente, como resultado de una reducción en la quema de combustibles fósiles.

{{< figure src="/img/whyweneedadepression.png" alt="Quema global de combustibles fósiles, incremento anual de 1990 al 2016" attr="Barry Saxifrage" attrlink="https://www.nationalobserver.com/2017/07/13/analysis/these-missing-charts-may-change-way-you-think-about-fossil-fuel-addiction" class="center" >}}

Desafortunadamente, aunque el cenit petrolero eventualmente reduzca el uso de combustibles fósiles y la actividad industrial colapse, todavía podría tardar varios años más en suceder. Mientras tanto, cada día quemamos combustibles fósiles que empujan el clima hacia el caos, asesinan a más de 16,000 seres humanos y llevan a la extinción a cientos de especies. Tenemos una responsabilidad de poner un alto a los combustibles fósiles de forma proactiva; no podemos sentarnos a esperar a que el sistema degrade la capacidad de vivir del planeta en lo que termina de agotar su capacidad para funcionar.

{{% /details %}}

{{% details better-life %}} 
{{< summary >}}
Los combustibles fósiles le dan conforts y elegancias a una minoría, pero a expensas de todos. **La vida será mejor sin combustibles fósiles**.
{{< /summary >}}

La idea de un "colapso" puede atemorizar, pero sólo significa una [simplificación rápida de la sociedad](https://www.goldonomic.com/tainter%20-%20collapse%20of%20complex%20societies.pdf), que en realidad [incrementará la calidad de vida](https://theanarchistlibrary.org/library/jason-godesky-thirty-theses#toc52) a largo plazo. Dicha simplificación es inevitable; nuestra forma de vida es insostenible, su complejidad depende de un incremento incesante de suministros de energía densa. En respuesta a la miríada de problemas ocasionados por la globalización, burocracia, desarrollo indetenible de infraestructura e imperialismo, muchas personas trabajan proactivamente hacia el colapso con el interés de fortalecer la capacidad local de generar y suministrar alimentos, agua, vivienda, energía, educación, economía y procesos de toma de decisiones. 

Es valioso que construyamos las bases para las estructuras locales ahora, pero no ganarán gran empuje hasta que los sistemas centralizados pierdan el poder que les suministra los combustibles fósiles. Los gobiernos suprimen constantemente los esfuerzos que buscan redistribuir el poder. Las presiones económicas obligan a las personas a participar en el sistema como un pequeño engrane de una máquina, restringiendo su libertad de explorar alternativas diferentes. El atractivo de las comodidades de alto consumo energético y dispositivos de tecnología avanzada resultarán irresistibles hasta el momento en que ya no estén disponibles.

Pero el conocimiento de cómo vivir una vida sostenible está disponible abiertamente. Conforme el flujo de combustibles fósiles se restrinja, la necesidad hará que recordemos el conocimiento previo dando pauta a innovaciones que brinden soluciones locales. Como [Tom Murphy](https://dothemath.ucsd.edu/2012/02/my-great-hope-for-the-future/) ha escrito, nos moveremos a un mejor futuro:

*Anticipa más:* Lectura, narraciones verbales, jardinería, conexión con la naturaleza, comunidad, pesca, tallado de madera, limonadas, sentarse a descansar, ajustes a las estaciones, mantas, calcetines de lana, suéteres, conexión con el amanecer y atardecer, gobernanza local, tiendas de familias, artesanías, crianza de cabras y gallinas, bicicletas, viajes por tren, pasteles que se enfrían en la ventana, música, cantar y tocar instrumentos musicales, captación de lluvias, mermeladas, desarrollo de talentos, capacidad para reparar, bienes de anaquel.

*Anticipa menos:* Esperar vuelos, manejar al trabajo, trabajos abstractos sin valor, Wal-Mart, comida rápida, centros comerciales, familias con cinco vehículos, cambio climático, dominación de bancos, ganancias capitalistas, basura desechable, correo basura, extinción de especies, cargos a la tarjeta, estar atorado en el tráfico, robo de identidad, ruido de carretera, comerciales, consumismo, artefactos digitales pasajeros, porquerías de plástico baratas, outsourcing, opulencia industrial, deudas de tarjeta de crédito.
{{% /details %}}

{{% details gentler-transition %}} 
{{< summary >}}
La población humana consume más de lo que el planeta puede dar, estamos en sobregiro poblacional y creciendo en números. Mientras tanto, nuestro impacto en la ecología global desacelera la capacidad del planeta a cada día. El colapso es inevitable. **Debemos poner un freno cuanto antes para tener una transición más suave**.
{{< /summary >}}

Estamos llevando a cabo un {{< pagelink "experimento de locos" "/gentler-transition/experiment-in-madness" >}}. Lo mejor sería que todos en el mundo recuperaran la cordura: una reducción en el consumo de bienes y disminución de la población humana que fuera agresiva, proactiva y unánime, buscando mitigar los daños del colapso, que quizás pueda evitar el desastre para las comunidades ecológicas y humanas. Pero no estamos optando por ese camino. Lejos de buscar el menor ingreso anual buscando un nivel de sostenibilidad global, inteligiblemente la gran mayoría de seres humanos compiten por acaparar una tajada más grande de pastel, el cual quieren que se haga más grande indefinidamente. Lejos de que cada familia contemple lograr una capacidad de carga global y planear los pasos para lograrla, los humanos se comportan de forma natural, como cualquier otra especie, [reproduciéndose de acuerdo a la cantidad de alimentos disponible](https://theanarchistlibrary.org/library/jason-godesky-thirty-theses#toc5).

Cada día, un total de [227,000 seres humanos nacen](https://esa.un.org/unpd/wpp/Publications/Files/WPP2017_Wallchart.pdf) y se unen a la población global. Al mismo tiempo, los combustibles fósiles reducen la habilidad del planeta de sostener a los que ya estamos aquí. El desarrollo industrial, manufactura, transporte, agricultura, pesca y deforestación erosionan el mantillo fértil de la tierra, desestabilizan el clima, envenenan con tóxicos el medio ambiente y causan el colapso de peces y toda vida silvestre. Mientras más rápido pongamos un alto a los combustibles fósiles podremos frenar nuestro sobregiro poblacional y el ajuste será menos doloroso.

{{% /details %}}


{{% frontheader "Como frenar los combustibles fósiles" how %}}

{{< figure src="/img/valve-turners-stop-fossil-fuels.jpg" caption="Activistas clausurando válvula" class="center section" >}}

{{% details stop-the-flows %}} 
{{< summary >}}
La sociedad industrial ha tenido décadas para hacer una transición voluntaria y acabar con los combustibles fósiles y los daños que sabemos provocan, pero la adicción es demasiado fuerte para abandonarlos por mera decisión. Es **momento de intervenir: debemos de cortar físicamente su flujo nosotros mismos**.  
{{< /summary >}}

Las consecuencias para humanos y no humanos son una gran amenaza que continúa empeorando. Como sociedad, hemos tenido este conocimiento por mucho tiempo, pero no hemos demostrado la habilidad para cambiar.

{{< figure src="/img/bp-global-fossil-fuel-burn.jpg" alt="Quema de combustibles fósiles de 1990 a 2016" attr="Barry Saxifrage" attrlink="https://www.nationalobserver.com/2017/07/13/analysis/these-missing-charts-may-change-way-you-think-about-fossil-fuel-addiction" class="center" >}}

El único año en el que el mundo redujo el consumo de combustibles fósiles fue en el 2009, como resultado de la recesión global. A menos que sea por circunstancias y fuerzas fuera de nuestro control, la economía quema más y más petróleo cada año. La única solución para su reducción reside en usar la fuerza.

Al abandonar nuestra fe en soluciones falsas, indirectas y permitidas por las autoridades como la transformación voluntaria masiva, acciones gubernamentales, energía verde y eficiencia energética, podemos liberar nuestra imaginación. Nuestro trabajo se transforma en una exploración directa de las estratégias y tácticas para detener físicamente los combustibles fósiles.

{{% /details %}}

{{% details attrition %}} 
{{< summary >}}
El movimiento ambientalista ha estado trabajando con una **estrategia para desgastar las capacidades de los enemigos que ha sido completamente inadecuada** para ir en contra de un sistema que tiene mucho mucho más personas y recursos. 
{{< /summary >}}

{{< figure src="/img/chessboard-nature-industry-s.jpg" caption="Fuerzas asimétricas, la estrategia de desgaste es inadecuada" link="/img/chessboard-nature-industry.jpg" class="right max50percentPDF" >}}

La [estrategia de desgaste](https://es.wikipedia.org/wiki/Guerra_de_desgaste) es una de ataque lento y prolongado, con el objetivo de debilitar las fuerzas de degradación ambiental hasta que cesen de funcionar. Reaccionamos de forma defensiva y nos oponemos a los proyectos destructivos uno por uno. Pero estamos perdiendo. Nuestras victorias ocasionales no lograrán realmente debilitar las fuerzas de degradación ambiental sino hasta que estas dejen por completo de funcionar. Pero estamos perdiendo. Nuestras victorias ocasionales no debilitan las instituciones que expanden el industrialismo; en el mejor caso, meramente desaceleramos un poco la tasa a la que se hacen más fuertes.

Para poder empezar a ganar en nuestra campaña de desgaste, no sólo debemos de bloquear toda la expansión industrial, pero debemos clausurar a la fuerza los oleoductos, plantas de energía y yacimientos petroleros. Pero incluso cuando logremos eso, nuestra estrategia seguiría siendo un fracaso. La estrategia de desgaste sólo funciona cuando uno detiene al oponente mientras se limitan las propias pérdidas ocasionales y éstas sean aceptables por mantenerse a niveles sostenibles. Pero no hay nada gradual, aceptable o sostenible en el aumento actual de CO<sub>2</sub> aunado a la reducción de la biodiversidad. La catástrofe es inminente. Incluso si tuviéramos la cantidad de personas adecuada para adoptar una estrategia de desgaste, no contamos con el tiempo.

Consideremos que a un nivel táctico más inmediato, ya nos basamos en el desgaste: anteponemos demandas, boicots, desinvertimos fondos de las grandes compañías, bloquemos oleoductos y nos encademos al equipo de construcción, con el propósito de que el proyecto no sea económicamente viable. Pero las inversiones de varios miles de millones de dólares pueden pagar con facilidad lo necesario para superar los obstáculos que pongamos en su camino, especialmente cuando corporaciones y gobiernos son capaces de predecir nuestras tácticas.

Nuestro trabajo ilustra por definición lo que es una lucha asimétrica. Como individuos y como movimiento, debemos escalar, ir más allá del desgaste minúsculo como única oposición. {{% /details %}}

{{% details strategy-principles %}} 
{{< summary >}}
A través de los siglos, el **análisis militar ha derivado principios de estrategias y tácticas**. Aunque nuestro objetivo para detener los combustibles fósiles es único, podemos aplicar lo que se ha aprendido a nuestro trabajo.
{{< /summary >}}

- Dirige cada operación hacia un **objetivo** claramente identificado, decisivo y alcanzable. Cada campaña y acción debe de avanzar hacia el objetivo final.
- Interven empleando una **economía de esfuerzos**. Elimina cualquier empeño que sea secundario.
- Aprovecha, reten y explota la **iniciativa**. En lugar de reaccionar defensivamente bajo los términos del oponente, actúa en la ofensiva.
- Usa tu iniciativa para elegir luchas donde puedas **aplicar tus fortalezas contra sus debilidades**. Concentra tus fuerzas en sus vulnerabilidades.
- **Sorprende** a tu oponente al atacar en un momento y lugar inesperados. El elemento sorpresa es un multiplicador de tus fuerzas; temporal pero poderoso.
- Acomete contra **múltiples objetivos simultáneamente** para obtener un mayor impacto y mantener a tu oponente fuera de equilibrio.
- Busca la **unidad de comando** para coordinar todas tus fuerzas hacia un efecto máximo. Si esto no es posible, asegura la unidad de esfuerzos compartiendo un mismo plan general.
- Mantén la **seguridad**, jamás permitas que tu oponente logre una ventaja inesperada. 
- Recaba **información**. Entiende cómo funciona la operación de los sistemas del oponente. Comprende lo que estás haciendo y los efectos posibles.
- Que tus acciones sean de una **duración corta** y luego retírate. Evita los enfrentamientos prolongados en posiciones fijas.


{{% /details %}}


{{% details cascading-failure %}} 
{{< summary >}}
Para detener los combustibles fósiles en esta lucha asimétrica, **debemos emplear una estrategia de fallos en cascada** para interrumpir un sistema tecnológico frágil.
{{< /summary >}}

El historiador militar y estratega, Liddell Hart, resume nuestro trabajo:

> "El objetivo de la gran estrategia debe ser descubrir y perforar el talón de Aquiles de la capacidad del gobierno opositor de hacer la guerra."

> "Un estratega debe pensar en términos de paralizar, no matar."

No necesitamos desmantelar cada fábrica, destruir cada excavadora y levantar todas las carreteras. Sólo necesitamos paralizar su infraestructura. La guerra contra el planeta, contra la mayoría de humanos en el globo y contra las generaciones del futuro es en gran medida posible por los combustibles fósiles. Para ir más allá de la estrategia de desgaste, debemos pensar en términos de sistemas, flujos, nodos y cuellos de botella. Debemos entender cómo el petróleo, carbón y gas son extraídos, transportados, procesados, distribuidos y quemados; y el punto en el que podemos intervenir para lograr un máximo impacto.

{{< figure src="/img/domino-effect-s.jpg" caption="Fallos en Cascada" link="/img/domino-effect.jpg" class="right max50percentPDF" >}}

Los sistemas industriales pueden sobrellevar la pérdida de un elemento o dos sin experimentar gran daño y reparar rápidamente los problemas. Pero estos sistemas están diseñados para la eficiencia y no la resiliencia. Cuando se logra romper una cantidad crítica de elementos simultáneamente, los fallos en cascada a través del sistema caen como una serie de dominó, provocando que más y más elementos fracasen. Los impactos aumentan exponencialmente mientras más persistan sus disturbios. Bajo las circunstancias adecuadas, todo el sistema fracasa hasta detenerse. Con las debidas acciones de seguimiento, es posible que no vuelva a recuperarse. I

#### Estrategia de Desgaste vs Fallos en Cascada

Estas son las tendencias generales; las campañas particulares pueden variar.

Característica           | Estrategia de Desgaste | Fallos en Cascada
-------------------------|---------------------|-----------------------------------------------------
Tipo de objetivo         | Proyecto nuevo en desarrollo | Infraestructura Operativa
Porqué de este objetivo     | Daños inmediatos causados por proyecto | Criticidad para el sistema
Objetivo de campaña      | Detener este proyecto   | Detener esta parte de la infraestructura, además de aquellas que dependan de ella, efecto dominó
Iniciativa               | Defensiva           | Ofensiva
Elemento sorpresa        | Mínimo              | Máximo
Rol del objetivo en el sistema  | Cualquiera                 | Cuello de botella o nodo de interconexión
Número de objetivos atacados | Uno a la vez, aislado | Múltiple, Ataque simultáneo para efecto sinérgico
Objetivos disponibles        | Muchos y sustituibles | Pocos y únicos
Tácticas                  | Bloqueos y ocupaciones | tácticas de guerrillas
Objetivos de las acciones          | Retrasar la finalización del proyecto. Reducir su valor económico. | Dañar o destruir infraestructura, retrasar o prevenir que se repare. Hacer que las operaciones sean físicamente imposibles.
Duración del efecto   | Corto             | Largo
Conocimiento necesario del sistema | Limitado           | Extensivo y profundo
Habilidades, planeación y precauciones necesarias | Menor                | Mayor
Acciones, recursos y personas necesarias | Muchas más           | Muchas menos
Apropiado para activistas expuestos | Elevado | Casi imposible
Apropiado para activistas clandestinos | Elevado | Elevado
Riesgos de las consecuencias para los activistas       | Mayor, si es abierto | Menor si hace subrepticiamente y con cuidado
Severidad de las consecuencias si son atrapados | Menor | Mayor
Respuesta del sistema          | Redirige alrededor del daño | Afectación desproporcionada

{{% /details %}}

{{% details civil-disobedience %}} 
{{< summary >}}
La **desobediencia civil tiene aplicaciones limitadas**. Aunque no pueden lograr un fallo en cascada del sistema, los bloqueos constantes, con suficientes participantes que van más allá de hacer meramente un acto simbólico pueden ganar algunas victorias de desgaste.
{{< /summary >}}

La desobediencia civil aumenta la oposición legal como lo son las peticiones, protestas y anteponer demandas. Pero gobiernos, corporaciones y el público en general jamás permitirán que los manifestantes interrumpan el quehacer habitual de los negocios al punto de lograr un fallo en cascada. En el mejor de los casos, tolerarán el desgaste hasta que puedan disipar los bloqueos de forma relativamente limpia.

Las acciones que sólo ocurren una vez tienen poco efecto para detener la quema de combustibles fósiles; el sistema tiene redundancia suficiente y amortiguamiento para no verse afectado por problemas temporales. Para que la desobediencia civil tenga un impacto material y no sólo simbólica, las campañas deben mantener la interrupción del sistema durante días, semanas o por más tiempo. Grandes cantidades de personas deben de participar en las primeras líneas y en roles de apoyo. La cobertura mediática y simpatía pública pueden reclutar voluntarios, pero sólo se puede captar la atención de forma limitada. Por ejemplo, en los EEUU, los oleoductos de Keystone XL y Dakota Access Pipeline se hicieron famosos, pero hay cientos de proyectos que permanecen sin ninguna atención.

En la Columbia Británica, el campamento Unis'tot'en ha logrado demostrar que la desobediencia civil puede ser exitosa, bloqueando el paso de los cuellos de botella de muchos oleoductos por años. Su base legal de oposición, combinado con una fuerte diseminación entre los movimientos comunitarios y apoyo del público, ha hecho que sea mucho más difícil para el gobierno que los haga a un lado. Las ocupaciones de infraestructura operativa pueden ser exitosos en casos donde la comunidad local perciba más daños que beneficios, como en el caso de Beleme, Nigeria, donde por un periodo de varios meses las personas [clausuraron los yacimientos petroleros](http://sweetcrudereports.com/2018/01/28/pandef-wades-shell-rivers-community-rift/) que generaban 45,000 barriles por día.

Antes de empezar con la desobediencia civil, considera si en el futuro quisieras trabajar en el movimiento clandestino para lograr un fallo en cascada. Lee cuidadosamente y contempla el {{< pagelink "cortafuegos obligatorio entre activistas del movimiento expuesto y clandestino" "/#aboveground-underground" >}}.

*Aunque parezca similar a la desobediencia civil, las "acciones directas no violentas" también incluyen el ecosabotaje con su potencial de ir más allá de la estrategia de desgaste. Ya que la "acción directa no violenta" conjunta las tácticas viables para las distintas estrategias, empleamos el término "desobediencia civil".*

{{% /details %}}

{{% details ecosabotage %}} 
{{< summary >}}
El **ecosabotage permite que los activistas tomen la defensiva** cuando se adopta una estrategia de fallos en cascada. Nivela los recursos limitados que tenemos contra la infraestructura enorme que en su mayor parte permanece desprotegida.
{{< /summary >}}

{{< figure src="/img/jessica-ruby-dapl-sabotage-s.jpg" caption="Sabotaje de la construcción del oleoducto DAPL" link="/img/jessica-ruby-dapl-sabotage.jpg" class="right max50percentPDF" >}}

La desobediencia civil tiene una historia noble dentro de los movimientos para el cambio social. Los infractores de la ley aceptan co orgullo las consecuencias de sus acciones con esperanza de ganarse los corazones y mentes necesarios para para la causa. Pero jamás convenceremos al público general para que apoye la reducción necesaria del 90% de la quema de combustibles fósiles, así que no debemos de temperar nuestras acciones efectivas por ellos. Los activistas que trabajan en el movimiento clandestino, que ataquen subrepticiamente y luego desaparezcan en la noche sin represalias, pueden aplicar por completo los {{< pagelink "principios de estrategia" "/#strategy-principles" >}} que hemos resumido más arriba.

Desafortunadamente, la mayoría de los actos de ecosabotage se han limitado a la estrategia de desgaste, con una selección de objetivos que sólo detienen el daño que estos mismo generan o a veces sólo por su valor simbólico. Cuando se ataca la infraestructura, es sólo en un lugar a la vez y los daños ocasionados son reparados con gran rapidez.

Para cambiar a una estrategia de fallos en cascada, los ecosaboteadores pueden estudiar la resistencia militar para aprender como atacar sistemas enteros de infraestructura.

{{% /details %}}

{{% details militant-resistance %}}
{{< summary >}}
En el Delta del Níger, la **resistencia militante ha frenado entre el 10 al 40% de la producción petrolera del país** desde el 2006, un impacto sin precedentes de en la historia del movimiento ambiental. El movimiento militante usa la violencia, pero han salvado muchas más vidas de las que han tomado.
{{< /summary >}}

La resistencia militante provee por mucho los ejemplos más exitosos de cómo funciona la estrategia de fallos en cascada y detener los combustibles fósiles. Lo mejor se puede observar en el Delta del Níger inundado en petróleo, donde los residentes sufren de los peores efectos de la contaminación y los derrames: el [aires, suelos y agua](https://www.unenvironment.org/explore-topics/disasters-conflicts/where-we-work/nigeria/environmental-assessment-ogoniland-report) dejan bosques, praderas, manglares y ríos sin la capacidad de sostener vida humana o no humana. Un estimado de [11,000 niños mueren cada año](http://www.cesifo-group.de/DocDL/cesifo1_wp6653.pdf) debido a derrames costeros de petróleo. A pesar de la inmensa riqueza obtenida de la extracción petrolera, la [población general no se beneficia](https://www.cia.gov/library/publications/the-world-factbook/geos/ni.html) la gran mayoría de los nigerianos viven en pobreza con la esperanza de vida más baja del mundo.

Durante décadas, las tácticas no violentas fracasaron en sus intentos de lograr justicia. En su lugar, en 1995 el ejército nigeriano y la Royal Dutch Shell conspiraron para ejecutar a nueve de los líderes del movimiento. El movimiento de resistencia escaló sus tácticas en respuesta y para el 2006 empleando tácticas como el [sabotaje y bombardeo](https://en.wikipedia.org/wiki/Movement_for_the_Emancipation_of_the_Niger_Delta) constantes de la infraestructura petrolera y edificios gubernamentales, secuestro de trabajadores extranjeros de la industria petrolera, el empleo de tácticas de guerrilla y el uso de ataques sorpresa usando lanchas de alta velocidad. Múltiples grupos clandestinos han estado seleccionando de manera sistemática y precisa objetivos que clausuren por completo la extracción petrolera o para frenar las reparaciones.

{{< figure src="/img/nigeria-crude-oil-extraction.png" caption="La resistencia militante freno la producción de crudo nigeriana de manera sostenida por 200-800,000 barriles de petróleo desde el 2006" class="center" >}}

Los militantes del Delta del Níger han asesinado a trabajadores de la industria petrolera, miembros del gobierno y militantes corporativos. De cualquier forma, un {{< pagelink "cálculo estimado" "/militant-resistance/niger-delta-lives-saved" >}} sugiere que los militantes han salvado entre 500 y 1900 vidas al mes por haber detenido la producción de 200-800,000 barriles de petróleo al día, lo cual supera por mucho el número de vidas que han tomado. Aunque, por supuesto queremos minimizar el uso de violencia, la resistencia militante está trabajando para un bien mayor.

{{% /details %}}

{{% details critical-infrastructure-systems %}} 
{{< summary >}}
Detener los combustibles fósiles no requiere violencia. Pero sí requiere que **usemos los limitados recursos disponibles para acometer contra objetivos de infraestructura crítica**. ]Debemos entender los sistemas industriales y sus vulnerabilidades.
{{< /summary >}}

La infraestructura crítica es aquella de la que dependen los combustibles fósiles para la extracción, transporte, procesamiento, distribución y combustión. Lo elementos obvios incluyen pozos, oleoductos, vías férreas, carreteras, tanques de almacenamiento, refinerías, plantas eléctricas y puertos. Menos visibles son los sistemas de administración, finanzas, telecomunicaciones y el suministro tipo "justo a tiempo". Todos tienen vulnerabilidades que pueden ser explotadas.

La desobediencia civil, tal como bloqueos de la construcción de oleoductos o vías férreas activas, puede tener un impacto en un subconjunto de la infraestructura crítica. El ecosabotage y la militancia pueden alcanzar más objetivos, con consecuencias más dañinas para el sistema y de mayor duración. Algunos ataques requieren de un conocimiento extenso y habilidades, pero muchos son sencillos y accesibles a cualquiera.

A menudo los militantes bombardean pozos y oleoductos. Los saboteadores (y a veces los borrachos) disparan con sus rifles y hacen hoyos en los oleoductos. Los ecosaboteadores perturban el tráfico de trenes al dañar vías conectándolas con los cables usados para pasar electricidad entre coches, saboteando cables, y vertiendo concreto o cortando árboles en las vías. Los activistas clandestinos cortan los cables de fibra óptica, derriban torres de radiocomunicaciones y queman transmisores. Los atacantes cibernéticos hacen que las computadoras y equipo industrial se dañen.

{{% /details %}}

{{% details electric-grid %}} 
{{< summary >}}
**La red eléctrica es particularmente vulnerable a los fallos en cascada**. Es posible que sea la infraestructura más crítica de la que dependen los combustibles fósiles.
{{< /summary >}}

La red eléctrida sustenta cada etapa del uso de combustibles fósiles, desde la extracción al transporte al procesamiento hasta la combustión. Los ataques dirigidos a la red podrían inhabilitar los dragaminas, detener estaciones de bombas y compresoras de oleoductos y gasoductos, frenar los trenes que transportan carbón, petróleo y los obturadores de refinerías y plantas de preparación de carbón o interrumpir la administración de proyectos. 

Debido a que la electricidad no se puede almacenar, los operadores de la red eléctrica deben de equiparar el suministro con la demanda, segundo a segundo. Así que si, por ejemplo, si el daño a una subestación eléctrica detiene las operaciones en un parque industrial frenando su consumo eléctrico, una central eléctrica en algún otro lugar debe de reducir su suministro o apagarse por completo por la demanda que se redujo. Esto lograría una situación ganar-ganar para el planeta, reduciendo tanto la actividad industrial como las emisiones de carbono.

La red eléctrica está expuesta. Globalmente, miles de kilómetros de líneas de transmisión de alto voltaje atraviesan áreas remotas y cientos de miles de subestaciones aisladas aparecen en los entornos menos custodiados. Los saboteadores pueden desactivar líneas y subestaciones con ataques tan sencillos como disparar con un rifle de caza, como se demostró en los experimentos de "Operación Corto Circuito" ([Operation Circuit Breaker](https://operationcircuitbreaker.wordpress.com/)) y por el ataque del 2013 a la subestación de [Metcalf, California](https://en.wikipedia.org/wiki/Metcalf_sniper_attack). Las torres de electricidad son bombardeadas o derribadas y las subestaciones son atacadas con incendios y ataques cibernéticos. 

Aunque la red está diseñada para funcionar a pesar de la pérdida de un nodo importante, a veces dos, una pérdida mayor podría sobrecargar el equipo restante. En un ejemplo de libro de texto de fallos en cascada de [fallos en cascada](https://en.wikipedia.org/wiki/Cascading_failure#Cascading_failure_in_power_transmission), despúes de llegar a la cantidad crítica, las subestaciones se van apagando cada vez más y más de forma automática para evitar daños mayores. 

La recuperación de un daño grave es difícil. Reemplazar los transformadores de alto voltaje puede tomar alrededor de 18 meses, ya que muchos están fabricados a la medida para un sitio específico y en rara ocasión cuentan con stock de reemplazo. Por su tamaño, el transporte de las unidades es difícil y lento, requiere de equipo especial y rutas de transporte planeadas que los vuelven más vulnerables a más ataques.

{{% /details %}}

{{% details target-selection %}} 
{{< summary >}}
La **selección cuidadosa de objetivos basada en infraestructura específica** es indispensable para iniciar una falla en cascada del sistema.
{{< /summary >}}

Un grupo puede analizar y seleccionar objetivos a muchos niveles antes de tomar. Puede decidir detener un sitio minero particular, lo cual los llevaría a apagar el suministro eléctrico del lugar. Entonces, podrían identificar una subestación particular para inhabilitarla y finalmente podrían planear sus acciones en torno a los transformadores vulnerables en la subestación.

A cada nivel del análisis, los activistas identifican cuellos de botella, aquellos elementos sin los cuales el sistema no puede funcionar. Los cuellos de botella obvios son puntos únicos de fallo, como el suministro eléctrico de este ejemplo. Aquí, los elementos están conectados con nodos redundantes, por lo que los activistas deben de buscar por los nodos con el mayor volumen y mayor cantidad de conexiones.

Las fuerzas de operaciones especializadas de los EEUU desarrollaron la matriz CRAVER para la selección de objetivos                :

<div class="carver">

<p><strong>C</strong>riticidad: ¿Qué tan importante es el elemento para el sistema?

<br><strong>A</strong>ccesibilidad: ¿Qué tan fácil acceso tiene?

<br><strong>R</strong>ecuperabilidad: ¿Qué tan rápido y con qué facilidad puede resumir su funcionalidad el sistema después de que se ha dañado un elemento?

<br><strong>V</strong>ulnerabilidad: ¿Que tan fácil resulta dañar al elemento con las tácticas y armas disponibles?

<br><strong>E</strong>fecto: ¿Qué efectos no deseados pueden resultar?

<br><strong>R</strong>econocibilidad: ¿Qué tan fácil resulta identificar el objetivo bajo las condiciones adversas como una noche lluviosa?
</p>
</div>

Aunque su compromiso y valentía no están en duda, la mayoría de los activistas ambientales clandestinos han seleccionado objetivos que son accesibles y vulnerables, pero que no son ni críticos ni difíciles de reemplazar. La siguiente ola de activistas ambientales que acepten a toda costa ser efectivos, aplicarán la matriz CARBER a los objetivos potenciales con el objetivo de iniciar un fallo en cascada del sistema. No sólo consideraran el acceso y daño de objetivos pero también el impacto material del éxito de sus acciones. 

Estudia un ejemplo del ejército de EEUU para seleccionar como objetivo un suministro eléctrico: {{< pagelink "Matriz CARVER" "/target-selection/carver" >}}.

{{% /details %}}


{{% details hit-and-run %}} 
{{< summary >}}
Los gobiernos y corporaciones no nos permitirán irrumpir la infraestructura crítica de forma expuesta. **Debemos usar táctica de ataques relámpago y emboscadas** para inducir un fallo en cascada.
{{< /summary >}}

De la doctrina militar existente, podemos aprender más que los {{< pagelink "principios de estrategia" "/#strategy-principles" >}}, pero de tácticas y operaciones. Aquellas empleadas en el combate de guerrillas son particularmente relevantes para nuestra propia lucha asimétrica:

- **Concentración de fuerzas**: Gobiernos y corporaciones manejan ejércitos enteros de fuerzas de seguridad tanto públicas como privadas. No podemos competir cara a cara, así que debemos coordinar a las personas con las que contamos para lograr una superioridad en números de forma local, es decir en un momento y lugar determinado podemos ser más poderosos temporalmente.
- **Planeación**: Hay que planear cada operación con cuidado y detalle. Mantén los planes y sus secuencias de forma sencilla para evitar malos entendidos y confusiones. Que sean plenas pero con la flexibilidad necesaria para incluir respuestas alternativas a contingencias.
- **Información**: Reúne información precisa y actualizada del objetivo hasta el momento de actuar.
- **Sorpresa**: El sistema sólo permite las tácticas que puede cooptar o absorber sin cambiar de ninguna forma fundamental sus negocios usuales. Debemos pensar y actuar con originalidad para lograr los objetivos de frenar los combustibles fósiles que ellos jamás permitirán.
- **Toma de decisiones rápida**: Debido a que no tenemos superioridad en números, una vez que se presenten la policía o fuerzas de seguridad privada, las acciones sorpresa deben lograr su propósito rápidamente.
- **Dispersión**: Después de una acción, los participantes deben de fundirse de nuevo en la población general para evitar represalias.
{{% /details %}}

{{% frontheader "Involúcrate" get-involved %}}

{{< figure src="/img/start-domino-effect.jpg" alt="Start the domino effect" class="center section" >}}

{{% details the-work %}} 
{{< summary no-details >}}
**Necesitamos que haya personas dispuestas a poner un alto a los combustibles fósiles**.  En general, esto requiere de activistas de primera línea, lealtad y apoyo material para ellos y diseminación de estrategia y tácticas.
{{< /summary >}}
{{% /details %}}

{{% details front-line-activists %}} 
{{< summary >}}
Los **activistas de primera línea detienen directamente los combustibles fósiles** a través de la desobediencia civil, ecosabotaje o ataques militantes. Necesitamos de todas las personas que podamos atraer.
{{< /summary >}}

Si estas en la posibilidad de tomar acción directa, el planeta te necesita desesperadamente. En especial necesita de personas que piensen y actúen en términos de fallos en cascada, de aquellos capaces de adoptar la postura clandestina y llevar acabo ecosabotage o acciones militantes.

Si no puedes participar en ataques clandestinos, la desobediencia civil puede ser valiosa. Aunque bajo la estrategia de desgaste a menudo limita las campañas expuestas, acometer contra objetivos particulares con tácticas inteligentes pueden darnos victorias reales. Quizás y con mayor importancia, las acciones expuestas proveen un medio a las personas para hacer conexiones y oportunidades de compartir la estrategia de fallos en cascada a través de medios con otros activistas y el público en general. 

{{% /details %}}

{{% details loyalty-material-support %}} 
{{< summary >}}
La mayoría de nosotros no puede estar en las primeras líneas por razones que son perfectamente válidas. Pero todos **podemos proveer con nuestra lealtad y apoyo material** a aquellos que están llevando a cabo el trabajo necesario.
{{< /summary >}}

Como mínimo, puedes adoptar el lema de "Ver y Callar", un código de silencio que proteja al ecosabotaje y la militancia. Fomenta a los demás la misma actitud.

De forma más proactiva, dale a los activistas de primera línea, especialmente a aquellos que trabajan en aislamiento clandestino, la valentía moral que llega de la validación de su comunidad. Escribe y habla para apoyar a los activistas de la resistencia y sus actos, en especial el ecosabotaje y militancia. Escribe cartas de solidaria a aquellos que estén capturados por llevar a cabo acciones efectivas. Explica el rango de tácticas que se considera posible y respetable.

El apoyo material no sólo fortalece la resolución de los activistas al demostrar lealtad, pero también ayuda físicamente a que su trabajo continúe. Si tienes los medios para contribuir ofrece suministros, comida, dinero, entrenamiento, transporte o casa. Todos podemos encontrar campañas valiosas de desobediencia civil o si conoces activistas clandestinos, puedes apoyar un trabajo aún más efectivo. Construye redes de seguridad y haz fondos de defensa para aquellos que queden presos en el sistema legal.

{{% /details %}}

{{% details spread-strategy %}} 
{{< summary >}}
Los medios de comunicación masiva jamás promoverán un alto a los combustibles fósiles. Como activistas y ciudadanos, a un nivel comunitario, debemos **correr la voz de las estrategias y tácticas efectivas**.
{{< /summary >}}

La estrategia de fallos en cascada la emplea el ejército alrededor del mundo por la sencilla razón de que funciona. Pero no se menciona en el movimiento ambiental. Depende de nosotros cambiar eso.

Cuando sea adecuado, comparte este análisis en grupo en conversaciones uno a uno, medios sociales, letras al editor, publicaciones de blogs, comentarios en páginas de internet y cualquier otro lugar relevante. Si apoyas o participas en movimientos de desobediencia civil, platica con activistas de primera línea y visitantes y transmite la estrategia en ruedas de prensa y declaraciones públicas. Aunque las acciones expuestas se limitan a la estrategia de desgaste, apaláncate de su visibilidad para fomentar una escalada hacia los fallos en cascada.

Por favor ayuda a Alto a los Combustibles Fósiles a difundir este análisis compartiendo este sitio con otros y distribuyendo {{< pagelink "material impreso" "/spread-strategy/printed-material" >}}.

{{% /details %}}

{{% details aboveground-underground %}} 
{{< summary >}}
Debe haber una **barrera de cortafuegos entre aquellos que trabajan de forma expuesta y clandestina** El movimiento necesita de personas en los dos movimientos, pero tienen roles y métodos de organización diferentes.
{{< /summary >}}

Los activistas expuestos usan tácticas legales además de la desobediencia civil. Trabajan de manera abierta y pública, a menudo llaman la máxima atención posible a sus acciones. Los activistas clandestinos operan ilegalmente, manteniendo un perfil bajo. Para no ser vistos por la ley, no pueden participar de forma segura en el activismo expuesto ni asociarse con los miembros expuestos.

Exhibir públicamente las convicciones y voluntad de romper la ley, o asociarse con dichos activistas hacen que uno se vuelva objetivo de investigaciones cuando suceden acciones clandestinas. Por ejemplo, la compañía de seguridad privada TigerSwan [vigiló a docenas de manifestantes de la campaña NO DAPL](https://theintercept.com/2017/08/26/dapl-security-firm-tigerswan-responded-to-pipeline-vandalism-by-launching-multistate-dragnet/) después del sabotaje de la Dakota Access Pipeline. Rápidamente, la compañía señaló a dos mujeres que eventualmente aceptaron responsabilidad. Ellas fueron sospechosas por su historial de desobediencia civil contra DAPL y otros proyectos dañinos.

{{% /details %}}

{{% details your-first-choice %}} 
{{< summary >}}
**Tu primera decisión es la más importante**: ¿Trabajarás en el movimiento expuesto o clandestino? Los activistas clandestinos pueden y deben protegerse a sí mismos desde el principio. 
{{< /summary >}}

Cuando te involucras en el movimiento expuesto no descartas que en un futuro puedas trabajar en el movimiento clandestino, pero sí aumenta los riesgos. Antes de apoyar públicamente el ecosabotaje, llevar a cabo desobediencia civil o unirte a un grupo radical expuesto, los activistas deben de considerar cuidadosamente si creen que en algún futuro podrían considerar operar clandestinamente.

Ya que aquellos que trabajan clandestinamente pueden usar tácticas mucho más efectivas que las del movimiento expuesto, los activistas deben pasar el tiempo necesario para tomar una decisión cuidadosa, sin irse directamente al trabajo expuesto sencillamente porque parece un camino más fácil u obvio. Mientras {{< pagelink "consideren su decisión" "/your-first-choice/aboveground-underground-considerations" >}}, deben de mantenerse con un perfil bajo, compartiendo sus deliberaciones con amigos y familiares de absoluta confianza y llevando a cabo investigaciones en el anonimato con buscadores y publicadores especiales.

{{% /details %}}

{{% details digital-security %}} 
{{< summary >}}
Los activistas, en especial aquellos que consideran acciones clandestinas, deben **usar herramientas digitales de seguridad** para mantener el anonimato y encriptar su comunicación.         
{{< /summary >}}

Aunque no tengas una necesidad urgente para proteger tu actividad en línea, acostumbrarse a mantener la privacidad ayuda a los activistas que sí la tienen. Si sólo aquellos que "tienen algo que ocultar" aseguran sus datas, su actividad llamará la atención. Conforme haya más personas que empleen herramientas digitales de seguridad, los activistas clandestinos se confundirán mejor con la multitud.

En [Prism Break](https://prism-break.org/) puedes obtener una lista comprensiva de las herramientas disponibles, y en [Freedom of the Press Foundation](https://freedom.press/training/) encontrarás muchos sitios a guías de seguridad. Los conceptos a tener en cuenta son:

- anonimidad vs pseudo anonimidad vs privacidad
- cifrado de extremo a extremo, *end to end encryption (E2EE)* vs cifrado de cliente a servidor

Herramientas gratuitas con las que puedes empezar:

- [Tor browser](https://www.torproject.org/projects/torbrowser.html): navegador web anónimo
- [Signal](https://signal.org/): E2EE para textos (para sustitución de programas convencionales) y llamadas de voz
- [Riot](https://riot.im): E2EE de mensajería instantánea
- [Wire](https://wire.com): E2EE de mensajería instantánea, llamadas de voz, conferencias, y video
- [Enigmail with Thunderbird](https://securityinabox.org/en/guide/thunderbird/linux/) o [Mailvelope](http://www.mailvelope.com/) or [GPG4USB](https://www.palloy.earth/How_to_use_GPG4USB/) o [ProtonMail](https://protonmail.com/): E2EE para correo electrónico con diferentes pros y contras.

{{% /details %}}

{{% details security-culture %}} 
{{< summary >}}
La **cultura de seguridad es más importante** que cualquier otra herramienta técnica. Todos los activistas deben aprender estas sencillos pero poderosos lineamientos generales.
{{< /summary >}}

La primera línea de defensa es la barrera cortafuegos entre activistas y grupos del movimiento expuesto y clandestino. Los agentes de la ley investigan y vigilan rutinariamente los manifestantes y sus asociados. Los activistas del movimiento clandestino deben permanecer a distancia del movimiento expuesto para salir del radar de la ley.

Comparte información en [base a "cómo se vuelva necesario"](https://crimethinc.com/2004/11/01/what-is-security-culture). Sólo aquellos que estén involucrados directamente deben tener conocimiento de las actividades ilegales o las personas que las llevarán a cabo. Los demás no deben preguntar, hablar o especular sobre la actividad ilegal o quienes son los individuos o grupos clandestinos que la ejecutarán. Las culturas de activistas deben arraigarse en un código de silencio.

Independientemente de que trabajes en el movimiento expuesto, clandestino o no te involucres de forma alguna en el movimiento, jamás hables con agentes de la ley de ninguna investigación. Conoce tus derechos, di que no amablemente y que no quieres hablar con ellos y termina conversación.

Ser descuidado es un peligro para activistas, pero también lo es la paranoia. Establece normas de comportamiento dentro de tu grupo y procesos para la resolución de conflictos a transgresiones que incluyan sexismo, racismo, abuso, creación de conflictos, división de grupo y violaciones a los cortafuegos con los límites al conocimiento necesario que cada quien debe respetar. No importa si hay o no un infiltrado; el grupo debe de atender la circunstancia y terminar con el comportamiento problemático.

Aquellos que participan en acciones clandestinas deben de estudiar con máxima rigurosidad la [seguridad operativa](https://blogsofwar.com/hacker-opsec-with-the-grugq/), y diseñar e implementar cuidadosamente medidas para tratar con adversarios.

{{% /details %}}

{{% details other-groups %}} 
{{< summary >}}
Si puedes puedes **atender las líneas primerisas**, el movimiento te necesita.
{{< /summary >}}

Desafortunadamente, no hay forma sencilla de encontrar y apoyar el activismo que va en contra de la infraestructura crítica. El movimiento no organizado en torno a supervisar y coordinar dicho trabajo, así que necesitarás preguntar dónde creas conveniente o buscar grupos y campañas en línea, luego investigar su efectividad para detener los combustibles fósiles. (Un papel importante de apoyo al movimiento para alguien que no puede estar en la línea de combate radica en organizar y actualizar información concerniente a las campañas activas y sus necesidades).

Sólo los activistas clandestinos pueden llevar a cabo algunas acciones útiles por cuenta propia. Si uno o dos se le unen puede permitir llevar a cabo acciones más sofisticadas y efectivas, pero encontrarlos puede ser difícil. Crimethinc ofrece estos puntos a considerar:

> Toma conciencia de cuánto tiempo tienes conociendo a las personas, que tanto se involucran en tu comunidad y sus qué tanto puedes rastrear su vida afuera de ella y que experiencias han tenido con otros. Los amigos con los que creciste, si todavía permanecen en tu vida, pueden ser los mejores compañeros para las acciones directas ya que estas familiarizado con sus fortalezas y debilidades y con la manera con la que enfrentan la presión, y porque sabes con certeza que son quienes dicen ser. Asegúrate de sólo confiar tu seguridad personal y la seguridad de tu proyecto con las cabezas del proyecto que comparten las mismas prioridades y compromisos y que no están buscando demostrar nada. A largo plazo, busca construir una comunidad de personas con amistades largas y experiencia actuando juntos, junto con las relaciones que puedan tener a otras comunidades.

{{% /details %}}

{{% details join-us %}} 
{{< summary >}}
Si puedes ayudar con investigación o difundiendo nuestro análisis, por favor **únete a nuestro trabajo en Alto a los Combustibles Fósiles**.  
{{< /summary >}}

Buscamos apoyar acciones efectivas investigando cuellos de botella y vulnerabilidades a la infraestructura crítica de los combustibles fósiles al igual que diseminar nuestro análisis y estrategia a aquellos que sean capaces de tomar acción. Si disfrutas la investigación, escribir o divulgación, ¡[ponte en contacto](mailto:contact@stopfossilfuels.org)!

{{% /details %}}

{{% details be-effective %}} 
{{< summary >}}
Así como debemos de aprender de la estrategia militar, debemos aprender de las lecciones que vienen del mundo de negocios para maximizar nuestra **efectividad personal y organizacional**.
{{< /summary >}}

Si no eres tan organizado, eficiente y productivo como deberías serlo, invierte tu tiempo para buscar un sistema de productividad que te funcione. La recompensa puede ser impresionante para individuos e incluso más dramática para grupos. Si los miembros son consistentes con sus responsabilidades y obligaciones, y son capaces de confiar el uno en el otro para que hagan lo que han dicho que van a hacer, la eficiencia del grupo incrementará en sinergia.

Los recursos y sistemas abunda, pero algunos lugares para empezar son:

- [Getting Things Done](https://gettingthingsdone.com/) {{< zineonly >}}— https://gettingthingsdone.com{{< /zineonly >}}
- [Deep Work](http://calnewport.com/) {{< zineonly >}}— http://calnewport.com{{< /zineonly >}}
- [Mark Forster](http://markforster.squarespace.com/) {{< zineonly >}}— http://markforster.squarespace.com{{< /zineonly >}}

{{% /details %}}
