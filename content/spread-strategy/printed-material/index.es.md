---
title: "Lecturas Publicadas"
linktitle: "Lecturas Publicadas"
slug: materiales-impresos
summary: 'Ayúdanos a diseminar nuestro análisis descargando, imprimiendo y distribuyendo nuestro material.'
date: 2018-07-14T05:58:02-10:00
weight: 50
featuredPic: zine.jpg
teaserpic: book-inserts-th.jpg
categoryslugs: ['spread-strategy'] # Translators: probably no need to change this.
categories: ["Difusión de Estrategia"] # set automatically based on categoryslugs
url: difusion-estrategia/materiales-impresos/ # set automatically based on first "categoryslug" plus "slug"
cclicense: false
draft: false
---

Por favor, ayúdanos a diseminar nuestro análisis descargando, imprimiendo y distribuyendo nuestro material.

## Marca hojas para libros

{{< figure src="download.png" alt="Download file" class="left download" link="https://stopfossilfuels.org/files/Stop-Fossil-Fuels-book-inserts.pdf" >}} Ayuda a "promocionar" Alto a los Combustibles Fósiles en tu biblioteca y librerías locales imprimiendo e insertando estos marcadores de hoja en los libros con temas ambientales y el futuro colectivo.

Busca en los mostradores libros populares y de tendencias que salen en la lista de best sellers de Amazon con temas de [medio ambiente y naturaleza](https://www.amazon.com/Best-Sellers-Books-Environment-Nature/zgbs/books/14459) o investiga y emplea las siguientes categorías:

{{< figure src="book-inserts.jpg" alt="Download file" class="right" link="https://stopfossilfuels.org/files/Stop-Fossil-Fuels-book-inserts.pdf" >}}

- 304.2 — Ecología humana, Colapso y Cenit petrolero
- 333.72 — Ambientalismo y Conservación
- 363.7 — Ambiente y Cambio Climático
- 574.5 — Ecología
- 631.58 — Permacultura, Orgánico y Comida Sustentable
- 693.9 — Construcciones Alternativas

Autores de ficción y libros seleccionados:

- Margaret Atwood
- Richard Powers — *{{< pagelink "The Overstory" "/ecosabotage/review-overstory-richard-powers" >}}*
- Daniel Quinn

## Librillo
{{< figure src="zine.jpg" alt="Zine of front page" class="right download" link="/files/No%20a%20los%20Combustibles%20F%c3%b3siles%20(es)-2up.pdf" >}}

{{< figure src="download.png" alt="Download file" class="left download" link="/files/No%20a%20los%20Combustibles%20F%c3%b3siles%20(es)-2up.pdf" >}}Imprime nuestra página principal como un librillo para las personas que sean capaces de leerlo de principio a fin. (Revisa el vínculo bajo "Leer más" si quieres el formato de página completa en PDF de la página principal.)

Pages are arranged for you to print two-sided, then fold in half to create a zine.

Imprime la página 1, luego imprime la página 2 en el reverso de la página 1. Presta atención a la orientación para que las páginas no esten de cabeza. Sigue estos pasos para el resto de las páginas del documento.

Algunas impresoras tienen la función que permite imprimirlo en su totalidad correctamente de forma automatizada.
