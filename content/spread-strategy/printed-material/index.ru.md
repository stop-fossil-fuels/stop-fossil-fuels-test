---
title: "Printed Material"
linktitle: "Printed Material"
slug: printed-material
summary: 'Please help disseminate our analysis by downloading, printing, and distributing material.'
date: 2018-07-14T05:58:02-10:00
weight: 50
featuredPic: zine.jpg
teaserpic: book-inserts-th.jpg
categoryslugs: ['spread-strategy'] # Translators: probably no need to change this.
categories: ["Spread the Strategy"] # set automatically based on categoryslugs
url: spread-strategy/printed-material/ # set automatically based on first "categoryslug" plus "slug"
cclicense: false
draft: false
---

Please help disseminate our analysis by downloading, printing, and distributing material.

## Book inserts

{{< figure src="download.png" alt="Download file" class="left download" link="https://stopfossilfuels.org/files/Stop-Fossil-Fuels-book-inserts.pdf" >}} Help "advertise" Stop Fossil Fuels at your local library and bookstore, by printing and inserting these bookmarks into books about our environment and collective future.

Look for displays of currently popular books, for titles on Amazon's [environment & nature](https://www.amazon.com/Best-Sellers-Books-Environment-Nature/zgbs/books/14459) best seller list, or browse relevant categories:

{{< figure src="book-inserts.jpg" alt="Download file" class="right" link="https://stopfossilfuels.org/files/Stop-Fossil-Fuels-book-inserts.pdf" >}}

- 304.2 — Human Ecology, Collapse, & Peak Oil
- 333.72 — Environmentalism & Conservation
- 363.7 — Environment & Climate Change
- 574.5 — Ecology
- 631.58 — Permaculture, Organics, & Sustainable Food
- 693.9 — Alternative Building

Fiction authors and books to target:

- Margaret Atwood
- Richard Powers — *{{< pagelink "The Overstory" "/ecosabotage/review-overstory-richard-powers" >}}*
- Daniel Quinn

## Zine
{{< figure src="zine.jpg" alt="Zine of front page" class="right download" link="/files/Stop Fossil Fuels (en)-2up.pdf" >}}

{{< figure src="download.png" alt="Download file" class="left download" link="/files/Stop Fossil Fuels (en)-2up.pdf" >}} Print our front page as a zine for people to be able to read from start to finish. (See the link under "Read More" if you want a normal full-page PDF of the front page.)

Pages are arranged for you to print two-sided, then fold in half to create a zine.

Print page 1, then print page 2 on the back of page 1. Pay attention to orientation so pages aren't upside down. Repeat for the rest of the document.

Some printers may have functionality to print it all correctly, automatically.

