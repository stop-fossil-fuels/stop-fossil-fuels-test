---
title: "Video & Audio Appearances"
linkTitle: "Video & Audio Appearances"
slug: video-audio-appearances
summary: "Watch and listen to members of Stop Fossil Fuels share analysis and strategy."
teaserpic: stop-fossil-fuels-presentation-santa-barbara.jpg
date: 2018-08-28T20:38:39-10:00
weight: 50
categoryslugs: ['spread-strategy'] # Translators: probably no need to change this.
categories: ["Spread the Strategy"] # set automatically based on categoryslugs
url: spread-strategy/video-audio-appearances/ # set automatically based on first "categoryslug" plus "slug"
cclicense: false
draft: false
---

{{< youtube iDIkZhMHiB8 >}}

