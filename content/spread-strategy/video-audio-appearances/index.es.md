---
title: "Apariciones en Vídeo y Audio"
linkTitle: "Apariciones en Vídeo y Audio"
slug: video-audio-apariciones
summary: "Mira vídeos y escucha audios de los miembros de Alto a los Combustibles Fósiles compartiendo el análisis y estrategia."
teaserpic: "stop-fossil-fuels-presentation-santa-barbara.jpg"
date: 2018-08-28T20:38:39-10:00
weight: 50
categoryslugs: ['spread-strategy'] # Translators: probably no need to change this.
categories: ["Difusión de Estrategia"] # set automatically based on categoryslugs
url: difusion-estrategia/video-audio-apariciones/ # set automatically based on first "categoryslug" plus "slug"
cclicense: false
draft: false
---

{{< youtube iDIkZhMHiB8 >}}
